#include <iostream>
#include <string> 
#include <cstring> 
#include <map>
#include <set>
#include <vector>

using namespace std;

vector<string> split(const string& i_str, const string& i_delim);


// Shubham Agrawal
// https://stackoverflow.com/a/57346888/6306059
// 
// Splits strings at delimiter
vector<string> split(const string& i_str, const string& i_delim)
{
    vector<string> result;

    size_t found = i_str.find(i_delim);
    size_t startIndex = 0;

    while (found != string::npos)
    {
        result.emplace_back(i_str.begin() + startIndex, i_str.begin() + found);
        startIndex = found + i_delim.size();
        found = i_str.find(i_delim, startIndex);
    }
    if (startIndex != i_str.size())
        result.emplace_back(i_str.begin() + startIndex, i_str.end());
    return result;
}

int main()
{

    // Friendly alternative to return/break
    bool shouldExit = false;

    // Stores question/answer combinations
    // Could use unsorted_map and vector for unsorted alternatives
    // #include <unordered_map>
    // #include <unordered_set>
    map< string, set<string> > dict;

    // Delimiter used to distinguish question from answer
    string questionDelimiter = "? ";
    // Delimiter used to distinguish answer from answer
    string answerDelimiter = "\" \"";


    do {

        string action = "";
        bool validAnswer = false;

        do {

            // Print "User-Interface" to console
            printf("What would you like to do?\n"
                "Add a question. (add)\n"
                "Ask a question. (ask)\n"
                "Exit the program. (exit)\n");

            printf("> ");

            // Ask for input from the user.
            getline(cin, action);

            if (action == "add" || action == "ask" || action == "exit") {
                validAnswer = true;
            }
            else {
                printf("\n\nOnly \"ask\", \"add\" and \"exit\" are valid options. Try again.\n");
            }

        } while (!validAnswer);


        printf("You typed \"%s\".\n", action.c_str());
        printf("\n\n");


        if (action == "exit") {
            // User wants to exit, then we do so.
            shouldExit = true;

            printf("\n\nThank you! Goodbye!\n\n");
        }
        else if (action == "add") {

            string userInput = "";

            // Ask for question input
            printf("Question: \n> ");
            getline(cin, userInput);

            printf("\n");

            // Seperate question from answers.
            // <question>? "<answer1>" "<answer2>" "<answerX>"
            // +1 for the "?"
            string question = userInput.substr(0, userInput.find(questionDelimiter) + 1);

            if (question.length() > 255) {
                printf("The question is too long. (>255)\n");
                continue;
            }

            // +2 for the extra space plus first "
            // -2 for the extra space plus first "  (as above)
            // -1 for the extra " in the end

            // <answer1>" "<answer2>" "<answerX>
            string rawAnswers = userInput.substr(question.length() + 2, (userInput.length() - question.length() - 2) - 1);


            string::size_type pos = 0;


            if (!dict.contains(question)) {
                dict[question] = {};
            }

            vector<string>::iterator itr1;

            // rawAnswers:       <answer1>" "<answer2>" "<answerX>
            // answerDelimiter:  " "
            // vector<string> a: ["<answer1>", "<answer2>", "<answerX>"]

            vector<string> a = split(rawAnswers, answerDelimiter);

            for (itr1 = a.begin(); itr1 < a.end(); itr1++) {
                const char* ans = (*itr1).c_str();
                if (strlen(ans) < 255) {
                    dict[question].insert(ans);
                }
                else {
                    printf("An answer was skipped, because it was too long."); 
                }
            }

            printf("Question: %s\n", question.c_str());
            //printf("Answers: %s\n", rawAnswers.c_str());

            set<string>::iterator itr2;

            int i = 0;
            for (itr2 = dict[question].begin(); itr2 != dict[question].end(); itr2++) {
                printf("Answer %i: %s\n", i++, (*itr2).c_str());
            }

            printf("\n\n");
        }
        else if (action == "ask") {


            string userInput = "";

            printf("Question: \n> ");
            getline(cin, userInput);

            printf("\n");


            if (dict.contains(userInput)) {

                set<string>::iterator itr;

                int i = 0;
                for (itr = dict[userInput].begin(); itr != dict[userInput].end(); itr++) {
                    printf("Answer %i: %s\n", i++, (*itr).c_str());
                }
            }
            else {
                printf("The answer to life, universe and everything is 42\n\n");
            }


            printf("\n");
        }



    } while (!shouldExit);


    return 0;
}